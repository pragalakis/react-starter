# React starter

This repo provides a quick way to start building apps

## What's inside?

- React - a frontend framework
- Snowpack - a build tool

## How to start?

- Clone this repo
- Remove the `.git` folder
- Install all the dependencies `npm install`
- Run the site locally: `npm start`
- Build the site: `npm run build`

## Alternative branches

- `fetch-on-load` branch: a basic app that does a `GET` request on load and has loading and error states.

To use it run: `git checkout fetch-on-load`

Start creating!
